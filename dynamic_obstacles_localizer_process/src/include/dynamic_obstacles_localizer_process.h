/*!********************************************************************************
 * \brief     Determination of the position of mobile obstacles.             
 * \authors   John Bradley
 * \copyright Copyright (c) 2020 Universidad Politecnica de Madrid
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#ifndef DYNAMIC_OBSTACLES_LOCALIZER_PROCESS_H_
#define DYNAMIC_OBSTACLES_LOCALIZER_PROCESS_H_

#include <ros/ros.h>
#include <ros/network.h>

#include <aerostack_msgs/SharedRobotPosition.h>
#include <aerostack_msgs/DynamicObstacles.h>
#include <aerostack_msgs/DynamicObstacle.h>

class DynamicObstaclesLocalizer
{

public:
  DynamicObstaclesLocalizer();
  ~DynamicObstaclesLocalizer();

  /*!************************************************************************
   *  \brief   This method opens the subscriptions needed by the process.
   ***************************************************************************/
  void openSubscriptions(ros::NodeHandle n);
  
  /*!************************************************************************
   *  \brief   This method checks if the subscriptions are successfully opened
   *  \return  Returns true if they're succesfully opened, false otherwise
   ***************************************************************************/
  bool isReady();
  
private:
  bool subscriptions_complete;
  bool already_exist;
  std::string shared_robot_position_topic;
  ros::Subscriber shared_robot_position_sub;
  aerostack_msgs::SharedRobotPosition shared_robot_position_msg;

  ros::NodeHandle p;
  std::string dynamic_obstacles_topic;
  ros::Publisher dynamic_obstacles_pub;
  aerostack_msgs::DynamicObstacle dynamic_obstacle_msg;
  aerostack_msgs::DynamicObstacles dynamic_obstacles_msg;
  aerostack_msgs::DynamicObstacles::_obstacles_type dynamic_obstacles_vector;
  aerostack_msgs::DynamicObstacles::_obstacles_type last_dynamic_obstacles_vector;
  std::vector<double> dim {1};
  
  /*!***********************************************************************************************
   *  \brief   This method saves aerostack_msgs::DynamicObstacles (drones)
   *           in a vector  when aerostack_msgs::SharedRobotPosition is received         
   **************************************************************************************************/
  void dynamicObstaclesLocalizerCallback(const aerostack_msgs::SharedRobotPosition::ConstPtr& msg);

};
#endif // DYNAMIC_OBSTACLES_LOCALIZER_PROCESS_H_
