/*!********************************************************************************
 * \brief     Determination of the position of mobile obstacles.            
 * \authors   John Bradley
 * \copyright Copyright (c) 2020 Universidad Politecnica de Madrid
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#include "../include/dynamic_obstacles_localizer_process.h"

DynamicObstaclesLocalizer::DynamicObstaclesLocalizer()
{
  subscriptions_complete = false;
  already_exist = false;
  dynamic_obstacles_vector;
}

DynamicObstaclesLocalizer::~DynamicObstaclesLocalizer()
{
}

void DynamicObstaclesLocalizer::openSubscriptions(ros::NodeHandle n)
{
  n.param<std::string>("shared_robot_position_topic", shared_robot_position_topic, "environment/shared_robot_positions_channel");
  shared_robot_position_sub = n.subscribe(shared_robot_position_topic, 1, &DynamicObstaclesLocalizer::dynamicObstaclesLocalizerCallback, this);  
  dynamic_obstacles_pub = n.advertise<aerostack_msgs::DynamicObstacles>("environment/dynamic_obstacles",1);
  subscriptions_complete = true;
}

bool DynamicObstaclesLocalizer::isReady()
{
  return subscriptions_complete;
}

void DynamicObstaclesLocalizer::dynamicObstaclesLocalizerCallback(const aerostack_msgs::SharedRobotPosition::ConstPtr& msg)
{ 
  shared_robot_position_msg = *msg;
  dynamic_obstacle_msg.shape.type = dynamic_obstacle_msg.shape.SPHERE;
  dynamic_obstacle_msg.shape.dimensions = dim;
  dynamic_obstacle_msg.position.x = shared_robot_position_msg.position.x;
  dynamic_obstacle_msg.position.y = shared_robot_position_msg.position.y;
  dynamic_obstacle_msg.name = shared_robot_position_msg.sender;
  dynamic_obstacle_msg.type = dynamic_obstacle_msg.DRONE;
  if (std::find(dynamic_obstacles_vector.begin(), dynamic_obstacles_vector.end(), dynamic_obstacle_msg) == dynamic_obstacles_vector.end())
  {
    for(std::vector<int>::size_type i = 0; i != dynamic_obstacles_vector.size(); i++) 
    {
      if (dynamic_obstacles_vector[i].name.compare(dynamic_obstacle_msg.name) == 0)
      {
        already_exist = true;
        dynamic_obstacles_vector[i] = dynamic_obstacle_msg;
        break;
      }
    }
    if(!already_exist)
    {
      dynamic_obstacles_vector.push_back(dynamic_obstacle_msg); 
    }
    dynamic_obstacles_msg.obstacles = dynamic_obstacles_vector;
    dynamic_obstacles_pub.publish(dynamic_obstacles_msg);
    already_exist = false;
  }
}
